from django.contrib import admin
from scanner.models import RegexPattern
from scanner.models import BlockedMessage

# Register your models here.
admin.site.register(RegexPattern)


@admin.register(BlockedMessage)
class BlockedMessageAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

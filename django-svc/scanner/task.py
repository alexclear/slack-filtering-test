import string
from django.utils.crypto import get_random_string
from celery import shared_task
from .dlp_api import invoke_dlp
from .dlp_api import invoke_dlp_disk
import logging
import urllib.request
from scanner.models import RegexPattern
from scanner.models import BlockedMessage

logger = logging.getLogger(__name__)


@shared_task
def scan_message(payload):
    logger.debug("We have text: " + payload['event']['text'])
    regex_patterns = RegexPattern.objects.all()
    scan_result = invoke_dlp(
        payload['event']['text'], map(
            lambda x: x.pattern, regex_patterns))
    logger.debug("Clean? " + str(scan_result['is_clean']))
    if not scan_result['is_clean']:
        blocked_message = BlockedMessage(
            client_msg_id=payload['event']['client_msg_id'],
            content=payload['event']['text'],
            pattern=scan_result['match'])
        blocked_message.save()
    if 'files' in payload['event']:
        logger.debug("We have got files!")
        for file_item in payload['event']['files']:
            logger.info(
                "Remote location: " +
                file_item['url_private_download'])
            local_path = "/tmp/" + get_random_string(16)
            local_filename, headers = urllib.request.urlretrieve(
                file_item['url_private_download'], local_path)
            logger.info("headers: " + str(headers))
            scan_result = invoke_dlp_disk(
                local_path, map(
                    lambda x: x.pattern, regex_patterns))
            if not scan_result['is_clean']:
                blocked_message = BlockedMessage(
                    client_msg_id=payload['event']['client_msg_id'],
                    content=payload['event']['text'],
                    pattern=scan_result['match'])
                blocked_message.save()

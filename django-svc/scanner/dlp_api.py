import logging
import re

logger = logging.getLogger(__name__)


def invoke_dlp(bulk, regexps):
    for regex in regexps:
        logger.debug("Regex: " + regex)
        match = re.match(regex, bulk)
        if match:
            logger.info("Found a match: " + regex)
            return {'is_clean': False, 'match': regex}
    return {'is_clean': True, 'match': None}


def invoke_dlp_disk(path, regexps):
    with open(path, 'r') as file_object:
        while True:
            line = file_object.readline()
            if not line:
                break
            for regex in regexps:
                logger.debug("Regex: " + regex)
                match = re.match(regex, line)
                if match:
                    logger.info("Found a match: " + regex)
                    return {'is_clean': False, 'match': regex}
    return {'is_clean': True, 'match': None}

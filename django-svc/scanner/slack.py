from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseServerError
import json
import logging
from .task import scan_message

logger = logging.getLogger(__name__)


def handle_webhook(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        logger.error(json.dumps(json_data))
        if 'type' in json_data and json_data['type'] == 'url_verification':
            return JsonResponse({'challenge': json_data['challenge']})
        elif 'type' in json_data and json_data['type'] == 'event_callback':
            if json_data['event']['type'] == 'message':
                scan_message.delay(json_data)
            return HttpResponse('OK')
        else:
            return HttpResponse('OK')
    else:
        return HttpResponseServerError("Method not allowed")

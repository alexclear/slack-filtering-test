from django.db import models


class RegexPattern(models.Model):
    pattern = models.CharField(max_length=200)


class BlockedMessage(models.Model):
    client_msg_id = models.CharField(max_length=200)
    content = models.CharField(max_length=4096)
    pattern = models.CharField(max_length=200)

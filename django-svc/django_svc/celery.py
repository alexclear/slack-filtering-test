import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_svc.settings')

app = Celery('django_svc')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

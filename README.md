## To start the app:
- Create a .env file with the following settings:
```
MYSQL_DATABASE=db_name
MYSQL_USER=user_name
MYSQL_PASSWORD=some_password
MYSQL_ROOT_PASSWORD=some_password
MYSQL_HOST=mysql
CELERY_BROKER_URL=redis://redis:6379/0
DJANGO_SUPERUSER_PASSWORD=some_password
DJANGO_SUPERUSER_USERNAME=admin
DJANGO_SUPERUSER_EMAIL=user@email.com
```
- Run `docker-compose up --build`
- Set up an external nginx proxy somewhere serving a real domain name over HTTPS
- Get an SSL cert for your domain (Let's Encrypt can do the trick)
- Register your app in your Slack Workspace
- Subscribe to incoming slack events
- Add a regex pattern (or several patterns) using the Django admin interface

## Bugs/caveats:
- Attached files are not scanned yet but it's technically possible
- I did not have enough time to write proper unit tests
- I did not bother to extract the DLP to a separate process beause it's useless at scale (I mean, to start a new process on every object to be scanned, using a long-lived scanner with an interface over a UNIX socket is more optimal, and BTW invoking an external command from Python is unnecessarily painful)
